<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
<%--<c:if test="${error}">Some wrong</c:if>--%>
${error}
<div class="form-style-2">
    <div class="form-style-2-heading">
        Please Sign Up!
    </div>
    <form method="post" action="/login">
        <label for="login">User name
            <input class="input-field" type="text" id="login" name="login">
        </label>
        <label for="password">Password
            <input class="input-field" type="password" id="password" name="password">
        </label>
        <label for="remember">
            <input type="checkbox" id="remember" name="remember">Remember me</label>
        <input type="submit" value="Sign Up">
    </form>
</div>
</body>
</html>