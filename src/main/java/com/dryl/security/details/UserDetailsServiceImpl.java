package com.dryl.security.details;

import com.dryl.dao.UserRepository;
import com.dryl.security.details.UserDetailsImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {
  @Autowired
  private UserRepository userRepository;
  @Override
  public UserDetails loadUserByUsername(String login) throws UsernameNotFoundException {
    return new UserDetailsImpl(userRepository.findUserByLogin(login)
        .orElseThrow(IllegalArgumentException::new));
  }
}
