package com.dryl.hateoas;

import com.dryl.model.User;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class UserTransfer {
  private String login;

  public static UserTransfer from(User user){
    return UserTransfer.builder().login(user.getLogin()).build();
  }
}
