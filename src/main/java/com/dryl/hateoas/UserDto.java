package com.dryl.hateoas;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.*;

import com.dryl.model.User;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.ResourceSupport;

public class UserDto extends ResourceSupport {

  User user;

  public UserDto(User user, Link selfLink) {
    this.user = user;
    add(selfLink);
  }

  public Long getUserId() {
    return user.getId();
  }

  public String getUserLogin() {
    return user.getLogin();
  }
}
