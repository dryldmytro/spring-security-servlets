package com.dryl.service;

import com.dryl.dao.UserRepository;
import com.dryl.model.Registration;
import com.dryl.model.Role;
import com.dryl.model.State;
import com.dryl.model.User;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class RegistrationService {
  private UserRepository userRepository;
  private PasswordEncoder passwordEncoder;

  public RegistrationService(UserRepository userRepository,
      PasswordEncoder passwordEncoder) {
    this.userRepository = userRepository;
    this.passwordEncoder = passwordEncoder;
  }

  public void registration(Registration registration){
    String password = passwordEncoder.encode(registration.getPassword());
    User user = User.builder().login(registration.getLogin())
        .password(password).role(Role.USER).state(State.ACTIVE).build();
    userRepository.save(user);
  }
}
