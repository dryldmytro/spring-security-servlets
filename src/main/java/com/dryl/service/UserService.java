package com.dryl.service;

import com.dryl.dao.UserRepository;
import com.dryl.model.User;
import java.util.List;
import javax.transaction.Transactional;
import org.springframework.stereotype.Service;

@Service
public class UserService {
UserRepository userRepository;

  public UserService(UserRepository userRepository) {
    this.userRepository = userRepository;
  }

  public List<User> getAllUsers(){
    return userRepository.findAll();
  }
  public User getUser(Long userId){
    User user = userRepository.findById(userId)
        .orElseThrow(()->new RuntimeException("User doesnt exist."));
    return user;
  }
  @Transactional
  public void createUser(User user){
    userRepository.save(user);
  }
  @Transactional
  public void updateUser(User uUser, Long userId){
    User user = userRepository.findById(userId).orElseThrow(RuntimeException::new);
    user.setLogin(uUser.getLogin());
    user.setPassword(uUser.getPassword());
    userRepository.save(user);
  }
  @Transactional
  public void deleteUser(Long userId){
    User user = userRepository.findById(userId).orElseThrow(RuntimeException::new);
    userRepository.delete(user);
  }
}

