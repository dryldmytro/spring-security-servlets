package com.dryl.controller;

import com.dryl.model.Registration;
import com.dryl.service.RegistrationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class RegistrationController {
  private RegistrationService registrationService;

  public RegistrationController(RegistrationService registrationService) {
    this.registrationService = registrationService;
  }

  @GetMapping(value = "registration")
  public String getRegistration(){
    return "registration";
  }
@PostMapping(value = "/registration")
  public String registration(Registration registration){
  registrationService.registration(registration);
  return "redirect:/login";
}
}
