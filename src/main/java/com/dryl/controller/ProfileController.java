package com.dryl.controller;

import com.dryl.hateoas.UserTransfer;
import com.dryl.security.details.UserDetailsImpl;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class ProfileController {
@GetMapping("/")
  public String getProfile(ModelMap model, Authentication authentication){
  UserDetailsImpl security = (UserDetailsImpl) authentication.getPrincipal();
  UserTransfer user = UserTransfer.from(security.getUser());
  model.addAttribute("user",user);
    return "profile";
  }
}
