package com.dryl.controller;

import com.dryl.service.UserService;

import javax.servlet.http.HttpServletRequest;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class LoginController {
  UserService userService;

  public LoginController(UserService userService) {
    this.userService = userService;
  }

  @GetMapping(value = "/login")
  public String getLoginPage(Authentication authentication,ModelMap model, HttpServletRequest request){
    if (authentication!=null){
      return "redirect:/"; //якщо юзер автифікований
    }
    if (request.getParameterMap().containsKey("error")){
      model.addAttribute("error","Some wrong");
    }
   return "login";
  }
}
